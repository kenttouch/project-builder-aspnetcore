# The Project 'Builder' #

This application is a training tool in using Asp.Net Core with Entity Framework using the Code First approach.

## What's Inside? ##

Inside the repository contains the project for using API through Asp.Net Core / EF Framework and
using a code first for database design.

Also, you need to know the basics of Asp.Net Core and Code First in using Entity Framework.

Objective of the project is to provide suggestions when it comes to building System Units or Personal Computers.

## Setup ##
Take note of the following files

* Startup.cs
* appsettings.json

For modifying the default connection to the database, edit the Startup.cs and modify the connection strings.

When adding a service to the application, kindly add them to the Startup.cs

## Aim of the Project ##
The aim of this project is to:

* Learn and understand the basics up to the advanced of the latest C# (v7.x)
* Learn and understand how the Asp.Net Core works and the API
* Learn and understand how to use EF Code First Approach with the latest EF version (v2.0.1)
* Understand the life cycle of the system, code refactoring, and usage of some syntaxes
* Learn how to build a pc from scratch, learning the technicalities of each component
* And lastly, provide a tool which helps the user on how to create a personal computer from scratch and within the user's budget

## Learning the basics ##
Here are the things that you need to know before getting into this project:

* Structural design of the system
* Proper naming Convention
* Code Reusability
* Less Processing time for the system

## Learn the rig building technicalities ##
These are the things that you need to know when it comes to rig technicalities:

* Relation of the different system to the other parts
* What makes a specific component fit/compatible to other unit parts
* Building a system unit without having to bottleneck the performance of the unit part

## Using the Code First ##
In creating, modifying or deleting any part of the database/table, use the steps below.

* After modifying the context file, go to Package Manager Console
* Type Add-Migration [Name Of Migration] ex. Add-Migration AddTable
* After Adding a Migration, a new folder named Migrations are created and the "AddTable" migration will be created
* Type in Update-Database in the console to apply the changes in the database. ex. Update-Database
* Modifications are now applied to your database.

## Future Aims/Goals ##
Coming soon... ;)