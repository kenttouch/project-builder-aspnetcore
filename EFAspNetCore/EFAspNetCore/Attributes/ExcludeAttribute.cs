﻿using System;

namespace EFAspNetCore.Attributes
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    internal sealed class ExcludeAttribute : Attribute
    {

    }
}
