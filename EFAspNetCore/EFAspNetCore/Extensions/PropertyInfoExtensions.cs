﻿using System.Reflection;

namespace EFAspNetCore.Extensions
{
    public static class PropertyInfoExtensions
    {
        /// <summary>
        /// Validates the PropertyInfo and returns false if the name is id, RegisterDate, and if it doesn't have get and set methods.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="includeId">Make id not included in validation</param>
        /// <param name="includeRegistDate">Make register date not included in validation</param>
        /// <param name="hasGetMethod">Make get method not included in validation</param>
        /// <param name="hasSetMethod">Make set method not included in validation</param>
        /// <returns></returns>
        public static bool SatisfyConditions(this PropertyInfo property, bool includeId = false, bool includeRegistDate = false, 
            bool hasGetMethod = true, bool hasSetMethod = true)
        {
            if (!includeId && property.Name == "id") return false;
            if (!includeRegistDate && property.Name == "RegisterDate") return false;
            if (hasGetMethod && IsObjectNull(property.GetMethod)) return false;
            if (hasSetMethod && IsObjectNull(property.SetMethod)) return false;

            return true;
        }

        /// <summary>
        /// Checks if object is null. Returns true if object is null, otherwise false.
        /// </summary>
        /// <param name="obj">Object to be checked.</param>
        /// <returns>Returns true  if object is null, otherwise false.</returns>
        public static bool IsObjectNull(this object obj)
        {
            if (obj == null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if the object or MethodInfo is null, returns true if any is null.
        /// </summary>
        /// <param name="obj">Object to be checked</param>
        /// <param name="method">MethodInfo to be checked</param>
        /// <returns></returns>
        public static bool IsObjectNull(this object obj, MethodInfo method)
        {
            if (obj == null || method == null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks two objects if any is null. Returns true if any is null.
        /// </summary>
        /// <param name="obj">First object to be checked</param>
        /// <param name="obj2">Second object to be checked</param>
        /// <returns></returns>
        public static bool IsObjectNull(this object obj, object obj2)
        {
            if (obj == null || obj2 == null)
            {
                return true;
            }
            return false;
        }
    }
}
