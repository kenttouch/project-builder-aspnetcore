﻿using EFAspNetCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFAspNetCore.Extensions
{
    internal static class EnumerableExtensions
    {
        internal static IEnumerable<T> NotExcluded<T>(this IEnumerable<T> list)
        {
            if(list.Count() < 1)
            {
                return null;
            }

            List<T> ts = new List<T>();
            foreach (var items in list)
            {
                Dictionary<string, T> st = new Dictionary<string, T>();
                foreach (var item in items.GetType().GetProperties())
                {
                    if (item.GetCustomAttributes(typeof(ExcludeAttribute), true).Length < 1)
                    {
                        if(item.GetValue(items) is T)
                        {
                            var value = (T)item.GetValue(items);
                            st.Add(item.Name, value);
                        }
                        try
                        {
                            var value = (T)Convert.ChangeType(item.GetValue(items), typeof(T));
                            st.Add(item.Name, value);
                        }
                        catch(InvalidCastException)
                        {
                            var value = default(T);
                            st.Add(item.Name, value);
                        }
                    }
                }
            }

            return ts;
        }
    }
}
