﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EFAspNetCore.Migrations
{
    public partial class AddTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GPUModel",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Accessories = table.Column<string>(nullable: true),
                    Base_Clock = table.Column<string>(nullable: false),
                    Boost_Clock = table.Column<string>(nullable: false),
                    Bus_Standard = table.Column<string>(nullable: true),
                    Cuda_Cores = table.Column<int>(nullable: false),
                    Dimensions = table.Column<string>(nullable: true),
                    Interface = table.Column<string>(nullable: true),
                    IsOC = table.Column<bool>(nullable: true),
                    IsRGB = table.Column<bool>(nullable: true),
                    Manufacturer = table.Column<int>(nullable: false),
                    ModelName = table.Column<string>(nullable: true),
                    NvidiaManufacturer = table.Column<int>(nullable: true),
                    OpenGL = table.Column<string>(nullable: true),
                    Price = table.Column<float>(nullable: false),
                    RadeonManufacturer = table.Column<int>(nullable: true),
                    RegisterDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ResolutionSupport = table.Column<string>(nullable: true),
                    Series = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    Video_Memory = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GPUModel", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GPUModel");
        }
    }
}
