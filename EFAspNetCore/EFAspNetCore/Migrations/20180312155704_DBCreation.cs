﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EFAspNetCore.Migrations
{
    public partial class DBCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GPUModel",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Accessories = table.Column<string>(nullable: true),
                    Base_Clock = table.Column<string>(nullable: true),
                    Boost_Clock = table.Column<string>(nullable: false),
                    Bus_Standard = table.Column<string>(nullable: false),
                    Cuda_Cores = table.Column<int>(nullable: false),
                    Dimensions = table.Column<string>(nullable: false),
                    Interface = table.Column<string>(nullable: true),
                    IsOC = table.Column<bool>(nullable: false),
                    IsRGB = table.Column<bool>(nullable: false),
                    Manufacturer = table.Column<int>(nullable: false),
                    ModelName = table.Column<string>(nullable: true),
                    NvidiaManufacturer = table.Column<int>(nullable: true),
                    OpenGL = table.Column<string>(nullable: false),
                    Price = table.Column<float>(nullable: false),
                    RadeonManufacturer = table.Column<int>(nullable: true),
                    RegisterDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ResolutionSupport = table.Column<string>(nullable: false),
                    Series = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    Video_Memory = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GPUModel", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "MemberDetails",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberDetails", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemberDetailId = table.Column<int>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GPUModel");

            migrationBuilder.DropTable(
                name: "MemberDetails");

            migrationBuilder.DropTable(
                name: "Members");
        }
    }
}
