﻿namespace EFAspNetCore.Enums.SystemUnit.CPU.AMD
{
    public enum EnumAMDManufacturers
    {
        ASUS = 0,
        Gigabyte = 1,
        MSI = 2,
        XFX = 3,
        Sapphire = 4
    }
}
