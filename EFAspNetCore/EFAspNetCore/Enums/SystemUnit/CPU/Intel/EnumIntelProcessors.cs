﻿namespace EFAspNetCore.Enums.SystemUnit.CPU.Intel
{
    /// <summary>
    /// All Intel Processors that is still produced
    /// </summary>
    public enum EnumIntelProcessors
    {
        IntelAtom,
        IntelCeleron,
        IntelXeon,
        IntelPentium2009,
        IntelCoreI3,
        IntelCoreI5,
        IntelCoreI7,
        IntelCoreI9
    }
}
