﻿namespace EFAspNetCore.Enums.SystemUnit.CPU.Intel
{
    public enum EnumIntelCodeNames
    {
        Airmont,
        Bonneli,
        Broadwell,
        Cannonlake,
        Clackamas,
        CoffeeLake,
        Gesher,
        Haswell,
        IvyBridge,
        KabyLake,
        Larrabee,
        Nehalem,
        Rockwell,
        Saltwell,
        SandyBridge,
        Silvermont,
        Skylake,
        Westmere,
        Yamhill
    }
}
