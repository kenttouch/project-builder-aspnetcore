﻿namespace EFAspNetCore.Enums.SystemUnit.CPU.Intel
{
    /// <summary>
    /// All Intel Series that is still produced, source = https://en.wikipedia.org/wiki/Comparison_of_Intel_processors
    /// </summary>
    public enum EnumIntelSeries 
    {
        #region For Intel Atom
        /// <summary>
        /// For Intel Atom
        /// </summary>
        Z5XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        Z6XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        N2XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        _2XX,
        /// <summary>
        /// For Intel Atom and Celeron
        /// </summary>
        _3XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        N4XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        D4XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        D5XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        N5XX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        D2XXX,
        /// <summary>
        /// For Intel Atom
        /// </summary>
        N2XXX,
        #endregion

        #region For Intel Celeron
        /// <summary>
        /// For Intel Celeron
        /// </summary>
        _4XX,
        /// <summary>
        /// For Intel Celeron
        /// </summary>
        _5XX,
        #endregion

        #region For Intel Xeon
        /// <summary>
        /// For Intel Xeon
        /// </summary>
        n3XXX,
        /// <summary>
        /// For Intel Xeon
        /// </summary>
        n5XXX,
        /// <summary>
        /// For Intel Xeon
        /// </summary>
        n7XXX,
        #endregion

        #region For Pentium 2009
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        E2xx0,
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        E5xxx,
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        E6xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        T2xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        T3xx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        T4xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        SU2xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        SU4xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        G69xx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        P6xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        U5xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        G6xx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        G8xx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        B9xx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        G2xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        _2xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        G3xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        _3xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        J2xx0, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        J3xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        N3xxx, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        G4xx0, 
        /// <summary>
        /// For Intel Pentium 2009
        /// </summary>
        _4xxx,
        #endregion

        #region Intel Series

        #region Intel I3
        /// <summary>
        /// For Intel Series I3
        /// </summary>
        i3_xxx, 
        /// <summary>
        /// For Intel Series I3
        /// </summary>
        i3_2xxx, 
        /// <summary>
        /// For Intel Series I3
        /// </summary>
        i3_3xxx, 
        /// <summary>
        /// For Intel Series I3
        /// </summary>
        i3_4xxx, 
        /// <summary>
        /// For Intel Series I3
        /// </summary>
        i3_61xx, 
        /// <summary>
        /// For Intel Series I3
        /// </summary>
        i3_63xx,
 
        #endregion

        #region Intel I5

        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_7xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_6xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_2xxx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_3xxx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_4xxx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_64xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_65xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_66xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_74xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_75xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_76xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_84xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i5_85xx, 
        /// <summary>
        /// For Intel Series I5
        /// </summary>
        i586xx,
        #endregion

        #region Intel I7

        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_6xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_7xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_8xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_9xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_2xxx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_37xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_38xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_47xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_48xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_58xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_59xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_67xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_68xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_69xx, 
        /// <summary>
        /// For Intel Series I7
        /// </summary>
        i7_7700K,
        #endregion

        #region Intel I7 (Extreme Edition)

        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_970, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_980, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_980x, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_990x, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_39xx, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_49xx, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_5820K, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_59xx_Extreme, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_6800K, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_6850K, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_6900K, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_6950X, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        _i5_7640X, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_7740X, 
        /// <summary>
        /// For Intel Series I7 (Extreme Edition)
        /// </summary>
        i7_7820X,
        #endregion

        #region Intel I9

        /// <summary>
        /// For Intel Series I9
        /// </summary>
        i9_7900X, 
        /// <summary>
        /// For Intel Series I9
        /// </summary>
        i9_7920X, 
        /// <summary>
        /// For Intel Series I9
        /// </summary>
        i9_7940X, 
        /// <summary>
        /// For Intel Series I9
        /// </summary>
        i9_7960X, 
        /// <summary>
        /// For Intel Series I9
        /// </summary>
        i9_7980XE
        #endregion

        #endregion
    }
}
