﻿namespace EFAspNetCore.Enums.SystemUnit.GPU.Radeon
{
    public enum EnumRadeonManufacturer
    {
        ASUS = 0,
        Gigabyte = 1,
        MSI = 2,
        XFX = 3,
        Sapphire = 4,




        Undefined = 1000
    }
}
