﻿namespace EFAspNetCore.Enums.SystemUnit.GPU
{
    public enum EnumGPUManufacturers
    {
        NVIDIA = 0,
        Radeon = 1
    }
}
