﻿namespace EFAspNetCore.Enums.SystemUnit.GPU.NVIDIA
{
    public enum EnumNvidiaManufacturer
    {
        ASUS = 0,
        MSI = 1,
        Acer = 2,
        Gigabyte = 3,
        Palit = 4,
        EVGA = 5,
        Zotac = 6,
        Galax = 7,


        Undefined = 1000
    }
}
