namespace EFAspNetCore.Enums
{
    public enum EnumYesNo
    {
        Yes = 1,
        No = 2
    }
}