using EFAspNetCore.Models.Members;
using EFAspNetCore.Models.UnitParts;
using Microsoft.EntityFrameworkCore;

namespace EFAspNetCore.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> optns) : base(optns) { }

        public DbSet<MemberModel> MemberModels { get; set; }
        public DbSet<MemberDetailsModel> MemberDetailModels { get; set; }

        #region Unit Parts
        public DbSet<GPUModel> GPUModels { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MemberModel>().ToTable("Members");
            modelBuilder.Entity<MemberDetailsModel>().ToTable("MemberDetails");
            modelBuilder.Entity<GPUModel>().ToTable("GPUModel").Property(prop=>prop.RegisterDate).HasDefaultValueSql("getdate()");
        }
    }
}