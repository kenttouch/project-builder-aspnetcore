using EFAspNetCore.Models.rootModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFAspNetCore.Models.Members
{
    public class MemberModel : RootModel
    {
        [Column(Order = 2)]
        public string UserName { get; set; }

        [Column(Order = 3)]
        public string Password { get; set; }
        
        
        
        public int MemberDetailId { get; set; }
    }
}