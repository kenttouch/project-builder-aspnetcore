﻿using System;

namespace EFAspNetCore.Models
{
    internal class DefaultAttribute : Attribute
    {
        public bool IsId { get; set; }
        public bool IsRegisterDate { get; set; }
        public bool IsUpdateDate { get; set; }

        public DefaultAttribute()
        {

        } 
    }
}