﻿using EFAspNetCore.Enums.SystemUnit.GPU;
using EFAspNetCore.Enums.SystemUnit.GPU.NVIDIA;
using EFAspNetCore.Enums.SystemUnit.GPU.Radeon;
using EFAspNetCore.Models.rootModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFAspNetCore.Models.UnitParts
{
    public class GPUModelDisplay : UnitPartsRoot
    {
        #region Basic Information
        public string ModelName { get; protected internal set; }
        public string Series { get; set; }
        public EnumGPUManufacturers Manufacturer { get; protected internal set; }

        public EnumNvidiaManufacturer? NvidiaManufacturer { get; protected internal set; }
        public EnumRadeonManufacturer? RadeonManufacturer { get; protected internal set; }

        public int CUDA_Cores { get; set; }

        private string _BaseClock;
        public string Base_Clock
        {
            get
            {
                return this._BaseClock + " MHz";
            }
            set
            {
                this._BaseClock = value;
            }
        }

        private string _BoostClock;
        public string Boost_Clock
        {
            get
            {
                return this._BoostClock + " MHz";
            }
            set
            {
                this._BoostClock = value;
            }
        }

        public string Bus_Standard { get; set; }
        public string OpenGL { get; set; }
        public string Video_Memory { get; set; }

        public string ResolutionSupport { get; set; }

        [NotMapped]
        public Dictionary<string, object> _Interface { get; set; }
        [NotMapped]
        public Dictionary<string, object> _Accessories { get; set; }

        public string Interface {
            get
            {
                string appendable = string.Empty;
                if(_Interface != null)
                {
                    foreach (var item in _Interface)
                    {
                        appendable += item + ", ";
                    }
                }

                return appendable;
            }
            set
            {

                var iValue = value.Split(',');
                if(iValue.Length > 0 && iValue[0] != "")
                {
                    _Interface = new Dictionary<string, object>();
                    for (int i = 0; i < iValue.Length; i++)
                    {
                        string[] details = (iValue[i].Split(':').Length < 1) ? null : iValue[i].Split(':');
                        if (details != null)
                        {
                            _Interface.Add(details[0], details[0] + ":" + details[1]);
                        }
                    }
                }
                
            }
        }

        public string Accessories {
            get
            {
                string appendable = string.Empty;
                if(_Accessories != null)
                {
                    for(int i = 0; i < _Accessories.Count; i++)
                    {
                        appendable += ((i + 1) != _Accessories.Count) ? _Accessories["Accessory " + (i + 1)] + "," : _Accessories["Accessory " + (i + 1)];
                    }                    
                }                

                return appendable;
            }
            set
            {

                var aValue = value.Split(',');
                if(aValue.Length > 0 && aValue[0] != "")
                {
                    _Accessories = new Dictionary<string, object>();
                    for (int i = 0; i < aValue.Length; i++)
                    {
                        _Accessories.Add("Accessory " + (i + 1), aValue[i]);
                    }
                }
                
            }
        }

        public string Dimensions { get; set; }
        #endregion

        #region GPU Features

        #endregion

    }
}
