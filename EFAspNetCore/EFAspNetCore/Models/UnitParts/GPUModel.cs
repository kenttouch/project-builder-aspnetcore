﻿using EFAspNetCore.Enums.SystemUnit.GPU;
using EFAspNetCore.Enums.SystemUnit.GPU.NVIDIA;
using EFAspNetCore.Enums.SystemUnit.GPU.Radeon;
using EFAspNetCore.Models.rootModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace EFAspNetCore.Models.UnitParts
{
    /// <summary>
    /// Contains the neccessary specifications of a Graphics Processor Card
    /// </summary>
    public class GPUModel : UnitPartsRoot
    {
        #region Private Members/Properties
        [Required]
        private int? _BaseClock;

        [Required]
        private int? _BoostClock;

        private int res_height;
        private int res_width;
        private int refreshRate;

        /* Dimensions */
        private string width_slot;
        private float width;
        private float height;
        private float length;

        private EnumGPUManufacturers _Manufacturer;

        [NotMapped]
        private Dictionary<string, object> _Interface { get; set; }

        [NotMapped]
        public string GPU_Manufacturer {
            get {
                EnumGPUManufacturers manufacturer = this._Manufacturer;
                return manufacturer.ToString();
            }
        }

        #endregion

        #region Basic Information

        #region Required Properties

        [Required]
        public EnumGPUManufacturers Manufacturer { set { _Manufacturer = value; } }

        public string Base_Clock
        {
            get
            {
                return (this._BaseClock.HasValue) ? this._BaseClock + " Mhz" : this._BaseClock.ToString();
            }
            set
            {
                this._BaseClock = GetIntFromString(value);
            }
        }

        [Required]
        public string Boost_Clock
        {
            get
            {
                return (this._BoostClock.HasValue) ? this._BoostClock + " Mhz" : this._BoostClock.ToString();
            }
            set
            {
                this._BoostClock = GetIntFromString(value);
            }
        }
        #endregion

        public string ModelName { get; protected internal set; }
        public string Series { get; set; }

        public EnumNvidiaManufacturer? NvidiaManufacturer { get; set; }
        public EnumRadeonManufacturer? RadeonManufacturer { get; set; }

        [Required]
        public int Cuda_Cores { get; set; }

        [Required]
        public string Bus_Standard { get; set; }

        [Required]
        public string OpenGL { get; set; }

        [Required]
        public string Video_Memory { get; set; }

        [Required]
        public string ResolutionSupport {
            get {
                return res_width + "x" + res_height + "@" + refreshRate + "Hz";
            }
            set
            {
                string[] arr = value.Split('@', 'x');
                res_width = int.Parse(arr[0]);
                res_height = int.Parse(arr[1]);
                refreshRate = GetIntFromString(arr[2]);
            }
        }

        public Dictionary<string, object> Interface_List { get { return this._Interface; } }

        public string Interface {
            protected get
            {
                string appendable = string.Empty;
                if(_Interface != null)
                {
                    for (int i = 0; i < _Interface.Count; i++)
                    {
                        appendable += ((i + 1) != _Interface.Count) ? _Interface["Interface " + (i + 1)] + "," : _Interface["Interface " + (i + 1)];
                    }
                    
                }

                return appendable;
            }
            set
            {

                var iValue = value.Split(',');
                if(iValue.Length > 0 && iValue[0] != "")
                {
                    _Interface = new Dictionary<string, object>();
                    for (int i = 0; i < iValue.Length; i++)
                    {
                        string[] details = (iValue[i].Split(':').Length < 2) ? null : iValue[i].Split(':');
                        if (details != null)
                        {
                            _Interface.Add("Interface " + (i + 1), details[0] + ":" + details[1]);
                        }
                    }
                }
                
            }
        }

        [Required]
        public string Dimensions {
            get {
                string dim = "Width: " + ((width < 1) ? width_slot  : width.ToString()) + " ";

                dim += height + " x " + length;

                return dim;
            }
            set {
                if(value.Contains("Width:"))
                {
                    value = value.Remove(value.IndexOf("W"), value.IndexOf(":") + 1);
                }
                string[] arr = value.Split(new char[] { ' ', 'x' }, StringSplitOptions.RemoveEmptyEntries);
                for(int i = 0; i < arr.Length; i++)
                {
                    switch(i)
                    {
                        case 0:
                            try
                            {
                                width = int.Parse(arr[i]);
                            }
                            catch(Exception x)
                            {
                                width_slot = arr[i];
                            }
                            break;
                        case 1:
                            height = float.Parse(arr[i]);
                            break;
                        case 2:
                            length = float.Parse(arr[i]);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        #endregion


        #region GPU Features

        #endregion

    }
}
