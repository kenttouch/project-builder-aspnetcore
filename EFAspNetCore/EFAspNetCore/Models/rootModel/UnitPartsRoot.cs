using EFAspNetCore.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace EFAspNetCore.Models.rootModel
{
    public class UnitPartsRoot : RootModel
    {
        protected internal string patternInt = @"[0-9.]";
        protected internal string p_removeString = @"[a-zA-Z.]";
        
        [Exclude]
        [Required]
        public float Price { get; set; }
        public string DisplayPrice { get { return "Php " + Price.ToString("#.##"); } }

        public bool IsOC { get; set; }
        public bool IsRGB { get; set; }

        [NotMapped]
        private Dictionary<string, object> _Accessories { get; set; }

        [Exclude]
        public string Accessories
        {
            get
            {
                string appendable = string.Empty;
                if (_Accessories != null)
                {
                    for (int i = 0; i < _Accessories.Count; i++)
                    {
                        appendable += ((i + 1) != _Accessories.Count) ? _Accessories["Accessory " + (i + 1)] + "," : _Accessories["Accessory " + (i + 1)];
                    }
                }

                return appendable;
            }
            set
            {

                var aValue = value.Split(',');
                if (aValue.Length > 0 && aValue[0] != "")
                {
                    _Accessories = new Dictionary<string, object>();
                    for (int i = 0; i < aValue.Length; i++)
                    {
                        _Accessories.Add("Accessory " + (i + 1), aValue[i]);
                    }
                }

            }
        }

        public Dictionary<string, object> Accessory_List { get { return this._Accessories; } }

        #region Functions

        /// <summary>
        /// Gets value and return numbers with strings. Ex. Input = 123xgk456, returns 123,456
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal string StringToIntComma(string value)
        {
            string s = Regex.Replace(value, patternInt, ",");

            if(s.Substring(s.Length - 2) == ",")
            {
                s.Substring(0, s.Length - 1);
            }

            return s;
        }

        /// <summary>
        /// Returns all integer from string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal int GetIntFromString(string value)
        {
            if(!Regex.IsMatch(value, p_removeString))
            {
                return 0;
            }

            return int.Parse(Regex.Replace(value, p_removeString, ""));
        }

        #endregion
    }
}