using EFAspNetCore.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace EFAspNetCore.Models.rootModel
{
    public class RootModel
    {
        #region Root Properties
        [Key]
        [Column(Order = 0)]
        public virtual int id { get; set; }

        [Default(IsRegisterDate = true)]
        public DateTime RegisterDate { get { return this._RegisterDate; } private set { this._RegisterDate = value; } }
        private DateTime _RegisterDate = DateTime.Now;

        [Default(IsUpdateDate = true)]
        public DateTime? UpdateDate { get { return _UpdateDate; } set { this._UpdateDate = value; } }
        private DateTime? _UpdateDate = null;
        #endregion


        #region Root Functions
        public T Update<T>(T oldData, T newData)
        {
            Dictionary<string, object> newObjects = new Dictionary<string, object>();

            foreach (var prop in GetAllProperties(newData))
            {
                if (prop.SatisfyConditions())
                {
                    if (!prop.IsObjectNull(prop.GetValue(newData)))
                    {
                        newObjects.Add(prop.Name, prop.GetValue(newData));
                    }
                }
            }

            foreach (var prop in GetAllProperties(oldData))
            {
                if(prop.SatisfyConditions())
                {
                    if(newObjects.ContainsKey(prop.Name))
                    {
                        prop.SetValue(oldData, newObjects.GetValueOrDefault(prop.Name));
                    }
                }
                if(prop.Name == "UpdateDate")
                {
                    prop.SetValue(oldData, DateTime.Now);
                }
            }

            return oldData;
        }

        protected internal PropertyInfo[] GetAllProperties<T>(T data)
        {
            return data.GetType().GetProperties();
        }

        #endregion
    }
}