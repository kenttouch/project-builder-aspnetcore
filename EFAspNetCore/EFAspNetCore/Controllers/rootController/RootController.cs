﻿using EFAspNetCore.Context;
using Microsoft.AspNetCore.Mvc;

namespace EFAspNetCore.Controllers.rootController
{
    public class RootController : Controller
    {
        protected DataContext _dataContext;
        
        /// <summary>
        /// Instantiate a new context class to be used by other controllers
        /// </summary>
        /// <param name="dataContext"></param>
        public RootController(DataContext dataContext)
        {
            this._dataContext = dataContext;
        }

    }
}
