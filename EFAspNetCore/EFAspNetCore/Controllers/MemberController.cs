﻿using EFAspNetCore.Context;
using EFAspNetCore.Controllers.rootController;
using EFAspNetCore.Models.Members;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EFAspNetCore.Controllers
{
    [Route("Api/Member")]
    public class MemberController : RootController
    {
        public MemberController(DataContext dataContext) : base(dataContext) { }

        [HttpGet]
        [Route("Display")]
        public IEnumerable<MemberModel> GetAllUsers()
        {
            var members = (this._dataContext.MemberModels == null) ? null : this._dataContext.MemberModels;

            return members;
        }

        //[HttpGet]
        //[Route("GetDetail")]
        //public IActionResult GetDetails(int id)
        //{
        //    var members = _dataContext.MemberModels.Where(m => m.id == id).Include("Member");
        //}

        [HttpPost]
        [Route("Register")]
        public IActionResult AddMember([FromBody]MemberModel model)
        {
            this._dataContext.MemberModels.Add(model);
            this._dataContext.SaveChanges();

            return Ok();
        }

        [HttpPut]
        [Route("ModifyMember")]
        public IActionResult UpdateMember(int id, [FromBody]MemberModel model)
        {
            var member = _dataContext.MemberModels.FirstOrDefault(m=>m.id == id);
            if(member == null)
            {
                return new JsonResult("Invalid User");
            }

            _dataContext.MemberModels.Update(member.Update(member, model));
            if(_dataContext.SaveChanges() > 0) {
                return new JsonResult("Success" + _dataContext.MemberModels);
            }
            else
            {
                return new JsonResult("Failed" + _dataContext.MemberModels);
            }

        }
        
        [HttpDelete]
        [Route("RemoveById")]
        public IActionResult DeleteMember(int id)
        {
            var member = _dataContext.MemberModels.FirstOrDefault(m => m.id == id);
            _dataContext.MemberModels.Remove(member);

            return (new JsonResult(_dataContext.SaveChanges()));
        }

        [HttpDelete]
        [Route("RemoveByCriteria")]
        public IActionResult DeleteMember([FromBody]MemberModel model)
        {
            JsonResult json = new JsonResult("");
            json.ContentType = "";
            return json;
        }
    }
}
