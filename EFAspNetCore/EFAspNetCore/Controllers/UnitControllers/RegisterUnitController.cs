﻿using EFAspNetCore.Context;
using EFAspNetCore.Controllers.rootController;
using EFAspNetCore.Models.UnitParts;
using Microsoft.AspNetCore.Mvc;

namespace EFAspNetCore.Controllers.UnitControllers
{
    [Route("Unit/Api/Register")]
    public class RegisterUnitController : RootController
    {
        public RegisterUnitController(DataContext dataContext) : base(dataContext) { }

        [HttpPost]
        [Route("Gpu")]
        public IActionResult RegisterGPU([FromBody]GPUModel model)
        {
            _dataContext.GPUModels.Add(model);

            return new JsonResult(_dataContext.SaveChanges());
        }

    }
}
