﻿using EFAspNetCore.Context;
using EFAspNetCore.Controllers.rootController;
using EFAspNetCore.Enums.SystemUnit.GPU;
using EFAspNetCore.Models.Members;
using EFAspNetCore.Models.UnitParts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EFAspNetCore.Controllers
{
    [Route("Api/UnitParts")]
    public class UnitPartsController : RootController
    {
        public UnitPartsController(DataContext dataContext) : base(dataContext) { }

        [HttpPost]
        [Route("AddGpu")]
        public IActionResult RegisterGPU([FromBody]GPUModel model)
        {
            _dataContext.GPUModels.Add(model);

            return new JsonResult(_dataContext.SaveChanges());
        }

        [HttpGet]
        [Route("GetAllGPU")]
        public IActionResult GetGPU()
        {
            var gpuUnits = _dataContext.GPUModels.ToList();

            return new JsonResult(gpuUnits);
        }

        [HttpGet]
        [Route("GetAllNvidia")]
        public IActionResult NvidiaGPU()
        {
            var nvidia = _dataContext.GPUModels.Where(model => model.Manufacturer == EnumGPUManufacturers.NVIDIA).ToList();

            return new JsonResult(nvidia);
        }
    }
}
