﻿using EFAspNetCore.Context;
using EFAspNetCore.Controllers.rootController;
using EFAspNetCore.Extensions;
using EFAspNetCore.Models.UnitParts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace EFAspNetCore.Controllers.UnitControllers
{
    [Route("Unit/Api/Display")]
    public class GetUnitController : RootController
    {
        public GetUnitController(DataContext dataContext) : base(dataContext) { }

        [HttpPost]
        [Route("Gpu")]
        public IActionResult GetGPU([FromBody]GPUModel model)
        {
            _dataContext.GPUModels.Add(model);

            return new JsonResult(_dataContext.SaveChanges());
        }

        [HttpGet]
        [Route("GpuAll")]
        public IEnumerable<GPUModel> GetAllGPU()
        {
            var gpuUnits = _dataContext.GPUModels.ToList().NotExcluded();

            return gpuUnits;// gpuDisplay;
        }

    }
}
